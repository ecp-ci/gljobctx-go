#!/usr/bin/env bash

echo "Verifying SPDX-License-Identifier on Go files..."

spdxID="// SPDX-License-Identifier: (Apache-2.0 OR MIT)"
goFiles=$(find . -type f -name "*.go" -not -path "./test/*")

for f in ${goFiles} ; do
  head -1 "${f}" | grep -q "${spdxID}" || echo "${f} missing identifier"
done
