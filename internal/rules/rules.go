// SPDX-License-Identifier: (Apache-2.0 OR MIT)

// Package rules maintains established validation for shared aspects relating
// to either Jacamar specifically or the ECP CI effort as a whole. All checks
// that have been defined realize the validator.Func interface for the
// github.com/go-playground/validator/v10 and are meant to be a component in
// any validation strategy.
//
// Example code:
//
//	 type Header struct {
//	     Alg string `json:"alg" validate:"len=5,alphanum"`
//	     Kid string `json:"kid" validate:"kid"`
//	     Typ string `json:"typ" validate:"eq=JWT"`
//	 }
//
//	 func Assert(h Header) error {
//		    v := validator.New()
//			_ = v.RegisterValidation("kid", rules.CheckKID)
//		    return v.Struct(h)
//	 }
//
// When using any rules within this package ensure that the desired value is
// optional or required. Many rules established are labeled as optional, meaning
// the lack of an expected value is acceptable and the maximum length should always
// be enforced via the go-playground/validator maximum structure flag. In these cases
// combining rules others found in validator package may be required. Additionally,
// always check the expected value type outlined in the function documentation. Type
// conversations are not handled and strict expectation are required to avoid failures.
package rules

import (
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt/v5"
)

const (
	usernameRegexp       = `^([a-zA-Z0-9][a-zA-Z0-9._-]{0,254})$`
	projectPathRegexp    = `^[a-zA-Z0-9][a-zA-Z0-9-_./+]+$`
	kidRegexp            = `^[a-zA-Z0-9\-_]+$`
	pipelineSourceRegexp = `^[a-zA-Z_\-]*$`
	urlRegexp            = `^[a-zA-Z0-9.\-+_:/@]*$`
)

func NewValidator() *validator.Validate {
	v := validator.New()

	_ = v.RegisterValidation("username", checkUsername)
	_ = v.RegisterValidation("projectPath", checkProjectPath)
	_ = v.RegisterValidation("kid", checkKID)
	_ = v.RegisterValidation("pipelineSource", checkPipelineSource)
	_ = v.RegisterValidation("ref", checkRef)
	_ = v.RegisterValidation("url", checkURL)
	_ = v.RegisterValidation("potentialURL", checkURL)

	return v
}

// CheckUsername optionally examines the provided username for validity based upon
// the GitLab server requirements while observing potentially egregious unix
// characters. Implements validator.Func for String types only.
var checkUsername validator.Func = func(v validator.FieldLevel) bool {
	if s, ok := v.Field().Interface().(string); ok {
		return regexp.MustCompile(usernameRegexp).MatchString(s)
	}

	return false
}

// CheckProjectPath ensures the values adheres to expectations of a GitLab group/project.,
// implements validator.Func. Path can contain only letters, digits, '_', '-' and '.'.
// Cannot start with '-', end in '.git' or end in '.atom'.
var checkProjectPath validator.Func = func(v validator.FieldLevel) bool {
	if s, ok := v.Field().Interface().(string); ok {
		return regexp.MustCompile(projectPathRegexp).MatchString(s)
	}

	return false
}

// CheckKID ensures a valid Key ID in a JWT header, implement validator.Func for string type.
var checkKID validator.Func = func(v validator.FieldLevel) bool {
	if s, ok := v.Field().Interface().(string); ok {
		return regexp.MustCompile(kidRegexp).MatchString(s)
	}

	return false
}

// CheckPipelineSource verifies the structure of a CI_PIPELINE_SOURCE string and no potentially
// illegal characters. The string itself is not checked ot ensure it is recognized as a source.
// Implements validator.Func for String types only.
var checkPipelineSource validator.Func = func(v validator.FieldLevel) bool {
	if s, ok := v.Field().Interface().(string); ok {
		return regexp.MustCompile(pipelineSourceRegexp).MatchString(s)
	}

	return false
}

// CheckRef enforces very limited checks regarding an incoming ref/type/environment
// for basic validity. This does not ensure that it is functional or accepted across
// all potential sources. Implements validator.Func for String types only.
var checkRef validator.Func = func(v validator.FieldLevel) bool {
	if s, ok := v.Field().Interface().(string); ok {
		matched := strings.Contains(s, `\`) ||
			strings.Contains(s, `..`) ||
			strings.Contains(s, `$`) ||
			strings.Contains(s, `|`)

		return !matched
	}

	return false
}

// CheckURL validates a potential url with valid characters only for
// the JWTs 'aud' or 'iss'. In older versions of the JWT 'aud' is not
// required and the 'iss' may not align with the requirements of a valid
// URL parse in Go. Implements validator.Func for String as []String
// types as it's possible to have multiple audiences.
var checkURL validator.Func = func(v validator.FieldLevel) bool {
	var urls []string

	if s, ok := v.Field().Interface().(string); ok {
		urls = []string{s}
	} else if cs, ok := v.Field().Interface().(jwt.ClaimStrings); ok {
		urls = cs
	} else {
		return false
	}

	for _, val := range urls {
		// https://www.ietf.org/rfc/rfc3986.txt
		// We should not accept any delimiting characters that may be used
		// outside the host or immediate path of a valid URL. In some older
		// cases a http prefix might not be included.
		if !regexp.MustCompile(urlRegexp).MatchString(val) {
			return false
		}
	}

	return true
}
