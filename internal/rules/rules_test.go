// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package rules

import (
	"regexp"
	"testing"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
)

func TestRegexp(t *testing.T) {
	tests := map[string]struct {
		r string
	}{
		"usernameRegexp": {
			r: usernameRegexp,
		},
		"projectPathRegexp": {
			r: projectPathRegexp,
		},
		"kidRegexp": {
			r: kidRegexp,
		},
		"pipelineSourceRegexp": {
			r: pipelineSourceRegexp,
		},
		"urlRegexp": {
			r: urlRegexp,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			_, err := regexp.Compile(tt.r)
			assert.NoError(t, err)
		})
	}
}

func TestCheckUserName(t *testing.T) {
	tests := map[string]struct {
		username string
		wantErr  bool
	}{
		"empty": {
			username: "",
			wantErr:  true,
		},
		"greater than 255 character": {
			username: "abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123",
			wantErr:  true,
		},
		"hyphen in middle": {
			username: "user-name01",
			wantErr:  false,
		},
		"alphanumeric": {
			username: "user123",
			wantErr:  false,
		},
		"underscore in middle": {
			username: "user_name01",
			wantErr:  false,
		},
		"space in middle": {
			username: "user name01",
			wantErr:  true,
		},
		"bash command": {
			username: "user$(env)",
			wantErr:  true,
		},
		"period starting": {
			username: ".username",
			wantErr:  true,
		},
		"period in middle": {
			username: "user.name",
			wantErr:  false,
		},
		"underscore starting": {
			username: "_username",
			wantErr:  true,
		},
		"hyphen starting": {
			username: "-username",
			wantErr:  true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()

			if err := v.Var(tt.username, "username"); (err != nil) != tt.wantErr {
				t.Errorf("CheckUsername() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		v := NewValidator()

		err := v.Var(1, "username")

		assert.Error(t, err)
	})
}

func TestProjectPath(t *testing.T) {
	tests := map[string]struct {
		path    string
		wantErr bool
	}{
		"empty": {
			path:    "",
			wantErr: true,
		},
		"begin with -": {
			path:    "-group/project",
			wantErr: true,
		},
		"invalid characters": {
			path:    "group/$(project)/1",
			wantErr: true,
		},
		"valid project path": {
			path:    "group/sub-group.1/my_project+test-1",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()

			if err := v.Var(tt.path, "projectPath"); (err != nil) != tt.wantErr {
				t.Errorf("CheckProjectPath() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		v := NewValidator()

		err := v.Var(1, "projectPath")

		assert.Error(t, err)
	})
}

func TestKID(t *testing.T) {
	tests := map[string]struct {
		id      string
		wantErr bool
	}{
		"empty": {
			id:      "",
			wantErr: true,
		},
		"invalid characters": {
			id:      "group/$(project)/1",
			wantErr: true,
		},
		"valid kid (gitlab.com/oauth/discovery/keys - 0)": {
			id:      "kewiQq9jiC84CvSsJYOB-N6A8WFLSV20Mb-y7IlWDSQ",
			wantErr: false,
		},
		"valid kid (gitlab.com/oauth/discovery/keys - 1)": {
			id:      "4i3sFE7sxqNPOT7FdvcGA1ZVGGI_r-tsDXnEuYT4ZqE",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()

			if err := v.Var(tt.id, "kid"); (err != nil) != tt.wantErr {
				t.Errorf("CheckKID() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		v := NewValidator()

		err := v.Var(1, "kid")

		assert.Error(t, err)
	})
}

func TestPipelineSource(t *testing.T) {
	tests := map[string]struct {
		source  string
		wantErr bool
	}{
		"empty": {
			source:  "",
			wantErr: false,
		},
		"invalid, alphanumeric": {
			source:  "EArXoCj8gmiAa8eZzPiR",
			wantErr: true,
		},
		"invalid, character encountered": {
			source:  "PG_buQjszuoCoxxayMZG$\\",
			wantErr: true,
		},
		"valid, lowercase and underscore encountered": {
			source:  "pg_buqjszuocoxxaymzg",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()

			if err := v.Var(tt.source, "pipelineSource"); (err != nil) != tt.wantErr {
				t.Errorf("CheckPipelineSource() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		v := NewValidator()

		err := v.Var(1, "pipelineSource")

		assert.Error(t, err)
	})
}

func TestRef(t *testing.T) {
	tests := map[string]struct {
		ref     string
		wantErr bool
	}{
		"empty": {
			ref:     "",
			wantErr: false,
		},
		"multiple periods": {
			ref:     "git...commit",
			wantErr: true,
		},
		"backslashes": {
			ref:     "//\\\\\\test",
			wantErr: true,
		},
		"valid ref": {
			ref:     "Git_commit-example-123",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()

			if err := v.Var(tt.ref, "ref"); (err != nil) != tt.wantErr {
				t.Errorf("CheckRef() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		v := NewValidator()

		err := v.Var(1, "ref")

		assert.Error(t, err)
	})
}

func TestURL(t *testing.T) {
	tests := map[string]struct {
		urlStr  string
		wantErr bool
	}{
		"empty": {
			urlStr:  "",
			wantErr: false,
		},
		"valid url": {
			urlStr:  "https://example.com",
			wantErr: false,
		},
		"valid IP address": {
			urlStr:  "http://127.0.0.1:5000",
			wantErr: false,
		},
		"invalid url in iss": {
			urlStr:  "localhost",
			wantErr: false,
		},
		"subdomain V1 iss": {
			urlStr:  "example.com/test",
			wantErr: false,
		},
		"IP address only": {
			urlStr:  "127.0.0.1",
			wantErr: false,
		},
		"invalid bash characters": {
			urlStr:  "https://example$(test).com",
			wantErr: true,
		},
		"email address": {
			urlStr:  "user@example.com",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()

			if err := v.Var(tt.urlStr, "url"); (err != nil) != tt.wantErr {
				t.Errorf("CheckURL() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		v := NewValidator()

		err := v.Var(1, "url")

		assert.Error(t, err)
	})
	t.Run("support jwt.ClaimsStrings", func(t *testing.T) {
		v := NewValidator()
		cs := jwt.ClaimStrings{
			"127.0.0.1",
			"https://example.com",
		}

		err := v.Var(cs, "url")

		assert.NoError(t, err)
	})
}
