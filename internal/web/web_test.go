// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package web

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/gljobctx-go/test/mocks/mock_web"
)

type webTest struct {
	tarURL  string
	method  string
	headers map[string]string
	v       interface{}

	httpC      *http.Client
	body       io.Reader
	mockClient httpClient

	assertError     func(*testing.T, error)
	assertInterface func(*testing.T, interface{})
	assertResponse  func(*testing.T, *http.Response)
}

// https://groups.google.com/g/golang-nuts/c/J-Y4LtdGNSw?pli=1
type ClosingBuffer struct {
	*bytes.Buffer
}

func (cb *ClosingBuffer) Close() error {
	return nil
}

func TestDefaultClient(t *testing.T) {
	t.Run("enforce default client type", func(t *testing.T) {
		got := DefaultClient()
		assert.IsType(t, clientManager{}, got)
		assert.Equal(t, defaultTimeout, got.(clientManager).web.(*http.Client).Timeout)
	})
	t.Run("10 second default timeout", func(t *testing.T) {
		// time.Duration is not a valid constant.
		assert.Equal(t, 10*time.Second, defaultTimeout)
	})
}

func TestCustomClient(t *testing.T) {
	t.Run("nil client provided", func(t *testing.T) {
		got := CustomClient(nil)
		assert.IsType(t, clientManager{}, got)
	})
	t.Run("custom client values observed", func(t *testing.T) {
		got := CustomClient(&http.Client{
			Transport: &http.Transport{
				TLSHandshakeTimeout: 5 * time.Second,
			},
		})
		assert.IsType(t, clientManager{}, got)
		assert.Equal(t, defaultTimeout, got.(clientManager).web.(*http.Client).Timeout)
		assert.Equal(t, 5*time.Second,
			got.(clientManager).web.(*http.Client).Transport.(*http.Transport).TLSHandshakeTimeout,
		)
	})
}

func createMockClient(
	ctrl *gomock.Controller,
	respTxt string,
	statusTxt string,
	statusCode int,
) httpClient {
	m := mock_web.NewMockhttpClient(ctrl)
	m.EXPECT().Do(gomock.Any()).Return(&http.Response{
		Status:     statusTxt,
		StatusCode: statusCode,
		Body:       &ClosingBuffer{bytes.NewBufferString(respTxt)}},
		nil).Times(1)
	return m
}

func Test_clientManager_NoAuthGetJSON(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	reqErr := mock_web.NewMockhttpClient(ctrl)
	reqErr.EXPECT().Do(gomock.Any()).Return(nil, errors.New("error message"))

	type Test struct {
		Hello string `json:"hello"`
	}

	tests := map[string]webTest{
		"request error": {
			tarURL:     "https://example.com",
			mockClient: reqErr,
			v:          &Test{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to complete http request: error message")
			},
		},
		"bad request (400)": {
			tarURL:     "https://example.com",
			mockClient: createMockClient(ctrl, `{"error":"400 Bad Request"}`, "400 Bad Request", http.StatusBadRequest),
			v:          &Test{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid response (400) received from http request: {\"error\":\"400 Bad Request\"}")
			},
		},
		"server error (500), empty response": {
			tarURL:     "https://example.com",
			mockClient: createMockClient(ctrl, "", "500 Internal Server Error", http.StatusInternalServerError),
			v:          &Test{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid response (500) received from http request")
			},
		},
		"successful request": {
			tarURL:     "https://example.com",
			mockClient: createMockClient(ctrl, `{"hello":"world"}`, "200 Successful", http.StatusOK),
			v:          &Test{},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertInterface: func(t *testing.T, i interface{}) {
				assert.IsType(t, &Test{}, i)
				assert.Equal(t, "world", i.(*Test).Hello)
			},
		},
		"bad json response": {
			tarURL:     "https://example.com",
			mockClient: createMockClient(ctrl, `"Invalid`, "200 Successful", http.StatusOK),
			v:          &Test{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to decode response, unexpected end of JSON input")
			},
		},
		"nil interface": {
			tarURL: "https://example.com",
			v:      nil,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to decode response, missing interface")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := clientManager{
				web: tt.mockClient,
			}.GetJSON(tt.tarURL, map[string]string{}, tt.v)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertInterface != nil {
				tt.assertInterface(t, tt.v)
			}
		})
	}

	t.Run("successful request with header", func(t *testing.T) {
		err := clientManager{
			web: createMockClient(ctrl, `{"hello":"world"}`, "200 Successful", http.StatusOK),
		}.GetJSON(
			"https://example.com",
			map[string]string{
				"HELLO": "WORLD",
				"TEST":  "TEST",
			},
			&Test{},
		)

		assert.NoError(t, err)
	})
}

func Test_clientManager_makeRequest(t *testing.T) {
	t.Run("NewRequest errors observed", func(t *testing.T) {
		cm := clientManager{
			web: &http.Client{Timeout: defaultTimeout},
		}
		_, err := cm.makeRequest(nil, "", "", map[string]string{}, nil)
		assert.EqualError(t, err, "unable to establish NewRequest: net/http: nil Context")
	})
}
