// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package version

import (
	"strings"
)

var (
	version   string
	goVersion string
	buildDate string
)

// Obtain returns properly formatted version (--version) details.
func Obtain() string {
	var sb strings.Builder

	sb.WriteString("Version: " + version + "\n")
	sb.WriteString("Go Version: " + goVersion + "\n")
	sb.WriteString("Built: " + buildDate + "\n")

	return sb.String()
}

// Version returns application's current build version.
func Version() string {
	return version
}
