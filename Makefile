SHELL := bash
.ONESHELL:
.DELETE_ON_ERROR:
.DEFAULT_GOAL := build
MAKEFLAGS += --no-builtin-rules

#
# Project & structure variables.
COVER_REPORT ?= cover.out
BUILDDIR ?= binaries/
ROOT_DIR ?= $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif
INSTALL_PREFIX ?= $(if $(DESTDIR),$(DESTDIR)/$(PREFIX),$(PREFIX))

#
# Version variables.
GOVERSION := $(shell go version | cut -c12- | awk '{ gsub (" ", "-", $$0); print}')
BUILDDATE := $(shell date -u +"%Y-%m-%dT%T%z")
ifndef GITCOMMIT
VERSION ?= $(shell cat VERSION)
else
VERSION ?= $(shell cat VERSION).pre.${GITCOMMIT}
endif
GOARCH := $(shell go env GOARCH)

#
# Go variables.
GOCMD := go
CGO_ENABLED = 0
LDFLAGS = -X ${INTERNAL_PKG}/version.version=${VERSION} \
		  -X ${INTERNAL_PKG}/version.goVersion=${GOVERSION} \
		  -X ${INTERNAL_PKG}/version.buildDate=${BUILDDATE} \
		  -s \
		  -w

.PHONY: test
test:
	GOEXPERIMENT=nocoverageredesign ${GOCMD} test -p 1 -coverprofile ${COVER_REPORT} -cover -tags netgo -timeout 2m -v ./... \
		&& ${GOCMD} tool cover -func=${COVER_REPORT}

.PHONY: test-quality
test-quality:
	golangci-lint run

.PHONY: test-security
test-security:
	gosec -exclude-dir=tools -exclude-dir=test  -exclude-dir=build ./...

.PHONY: check-spdx
check-spdx:
	bash ./test/scripts/check_spdx.bash

.PHONY: check-mod
check-mod:
	${GOCMD} mod tidy
	git diff --exit-code -- go.sum

.PHONY: coverage
coverage:
	${GOCMD} tool cover -func=${COVER_REPORT}

.PHON: clean
clean:
	rm -f ${COVER_REPORT}

.PHONY: mocks
mocks:
	mockgen -source=internal/web/web.go \
		-destination=test/mocks/mock_web/mock_web.go \
		-package=mock_web
	mockgen -source=gljobctx.go \
		-destination=test/mocks/mock_gljobctx/mock_gljobctx.go \
		-package=mock_gljobctx

.PHONY: version
version: #M Display current version and Git details.
	@echo Version: ${VERSION}
	@echo Git Commit: ${GITCOMMIT}
	@echo Git Branch: ${GITBRANCH}
	@echo Go Version: ${GOVERSION}
	@echo Built: ${BUILDDATE}
