// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package gljobctx

import (
	"crypto/rsa"
	"math/big"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
)

func TestClaims_verifyJobID(t *testing.T) {
	tests := map[string]jwtTest{
		"no id in claims": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "missing Job ID (job_id)")
			},
		},
		"stolen jwt": {
			jobID: "123",
			claims: Claims{
				JobID: "456",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "stolen JWT detected")
			},
		},
		"valid id": {
			jobID: "10001",
			claims: Claims{
				JobID: "10001",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.claims.verifyJobID(tt.jobID)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_job_parseClaims(t *testing.T) {
	n := new(big.Int)
	n, _ = n.SetString("23648271657106453702098841732073788291532737392306604587860650446757306564466381364830857011274527540070651468113486960514163897275777177717466786889375898552139336877707065290374442122684104815548349922622578914529273358894676394378991537856270829263472326566033341246002906926997050938988620778948738899041359146495682618114997520066322627837153551839387802883674510090916769974408762318103732102388373033874929324912398478133782307860465417827258830278539823429425375765133083338284554227468113368339505638355776984713752572292451297353783196607813958264972715696721578480782668352225529538321595051179616947854491", 10)
	public := &rsa.PublicKey{
		N: n,
		E: 65537,
	}

	tests := map[string]jwtTest{
		"successfully parse jwt and claims": {
			job: job{
				encoded: generateJWT(functional),
				opts: Options{
					JobID: "123",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertClaims: func(t *testing.T, c Claims) {
				c.OverrideClaims = OverrideClaims{}

				assert.Equal(t, Claims{
					UserLogin:      "user",
					ProjectPath:    "group/project",
					JobID:          "123",
					NamespaceID:    "1001",
					NamespacePath:  "group",
					ProjectID:      "2002",
					UserEmail:      "user@example.com",
					PipelineSource: "",
					PipelineID:     "456",
					UserID:         "789",
					Ref:            "066ac420faf6a3e96561eca998796af07480a64e",
					RefProtected:   "true",
					RefType:        "test",
				}, c)
			},
		},
		"successfully parse jwt with external identity claim": {
			job: job{
				encoded: func() string {
					newClaims := functional
					newClaims.UserIdentities = []map[string]string{{
						"provider":   "github",
						"extern_uid": "1",
					}}
					return generateJWT(newClaims)
				}(),
				opts: Options{
					JobID: "123",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertClaims: func(t *testing.T, c Claims) {
				c.OverrideClaims = OverrideClaims{}

				assert.Equal(t, Claims{
					UserLogin:      "user",
					UserIdentities: []map[string]string{{"provider": "github", "extern_uid": "1"}},
					ProjectPath:    "group/project",
					JobID:          "123",
					NamespaceID:    "1001",
					NamespacePath:  "group",
					ProjectID:      "2002",
					UserEmail:      "user@example.com",
					PipelineSource: "",
					PipelineID:     "456",
					UserID:         "789",
					Ref:            "066ac420faf6a3e96561eca998796af07480a64e",
					RefType:        "test",
					RefProtected:   "true",
				}, c)
			},
		},
		"value size validation failed": {
			job: job{
				encoded: func() string {
					newClaims := functional
					newClaims.RefType = "SL60TIVL8Q0SJZFPE75KB2QUKPCHWUC2KCNL9MM8C7OLBADG3ZJKI1QLJUMNWECAORD220A3JXY"
					return generateJWT(newClaims)
				}(),
				opts: Options{
					JobID:               "123",
					ClaimsEnvValidation: true,
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "Error:Field validation for 'RefType' failed on the 'max' tag")
				}
			},
		},
		"different job id provided via jwt and runner": {
			job: job{
				encoded: generateJWT(functional),
				opts: Options{
					JobID: "456",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "stolen JWT detected")
			},
		},
		"invalid job claims in jwt": {
			job: job{
				encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6InNsdWcvdGVzdCIsImpvYl9pZCI6MTIzLCJleHAiOjMyNDkxODIwMTYwfQ.GCZ02BpMdbUkXrXCd1zBZWWco7p8QMPXjRdxCb9ICGeGxxtxF4eUeYgjz-5XFLyLUuL3JbxuT5atm8ckMR_DUr6Ami6knDGFvWUjHD007RT9lUghBdqDOp--2eTNRbI4ivwU87oC-sfKx9F9X9CTTN85fLQesx6STm7GQRzHMFJCmMN2p0fdxAZMQcmoSJioe7YYLxPvNyu4ylHYZHA6WJ0FzEwUj23R1Cwm74H59b4O47DSILH8T49lEJRvp_WGYkFPhREnldYcQtWX0BJHL-hrpYfUy6WjQdviPj_X7jnKmsG5madX_dkvWDRRQ_0SLfeqiQlG6pqpAEjjMGlnoA",
				opts: Options{
					JobID: "123",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(
					t,
					err,
					"cannot unmarshal number into Go struct field Claims.job_id of type string",
				)
			},
		},
		"expired jwt provided": {
			job: job{
				encoded: func() string {
					newClaims := functional
					newClaims.ExpiresAt = &jwt.NumericDate{
						time.Now().Add(-30 * time.Minute),
					}
					return generateJWT(newClaims)
				}(),
				opts: Options{
					JobID: "123",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "token is expired")
			},
		},
		"empty jwt payload": {
			job: job{
				encoded: generateJWT(Claims{}),
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "missing expiration (exp)")
			},
		},
		"missing jwt nbf": {
			job: job{
				encoded: func() string {
					newClaims := functional
					newClaims.NotBefore = nil
					return generateJWT(newClaims)
				}(),
				opts: Options{
					JobID: "123",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "missing not valid before (nbf)")
			},
		},
		"incorrect signing method": {
			job: job{
				encoded: func() string {
					token := jwt.NewWithClaims(jwt.SigningMethodHS256, functional)
					str, _ := token.SignedString([]byte("secret"))
					return str
				}(),
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "unexpected signing method: HS256")
			},
		},
		"missing required ref": {
			job: job{
				encoded: func() string {
					newClaims := functional
					newClaims.Ref = ""
					return generateJWT(newClaims)
				}(),
				opts: Options{
					JobID:               "123",
					ClaimsEnvValidation: true,
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "validation for 'Ref' failed")
			},
		},
		"missing jwt id (jti)": {
			job: job{
				encoded: func() string {
					newClaims := functional
					newClaims.JwtId = ""
					return generateJWT(newClaims)
				}(),
				opts: Options{
					JobID:               "123",
					ClaimsEnvValidation: true,
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "validation for 'JwtId' failed on the 'required' tag")
				}
			},
		},
		"incorrect aud": {
			job: job{
				encoded: generateJWT(functional),
				opts: Options{
					JobID:            "123",
					RequiredAudience: "gitlab.example.com",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "token has invalid audience")
			},
		},
		"correct aud": {
			job: job{
				encoded: generateJWT(functional),
				opts: Options{
					JobID:            "123",
					RequiredAudience: "example.com",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"jwt provided before nbf": {
			job: job{
				encoded: func() string {
					newClaims := functional
					newClaims.NotBefore = &jwt.NumericDate{
						time.Now().Add(30 * time.Minute),
					}
					return generateJWT(newClaims)
				}(),
				opts: Options{
					JobID: "123",
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "token is not valid yet")
			},
		},
		"successfully validate jwt and claims": {
			job: job{
				encoded: generateJWT(functional),
				opts: Options{
					JobID:               "123",
					ClaimsEnvValidation: true,
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid additional aud provided": {
			job: job{
				encoded: func() string {
					newClaims := functional
					newClaims.Audience = jwt.ClaimStrings{
						"https://gitlab.example.com",
						"A random sentence!",
					}
					return generateJWT(newClaims)
				}(),
				opts: Options{
					JobID:               "123",
					ClaimsEnvValidation: true,
				},
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "Field validation for 'Audience' failed")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.job.parseClaims(tt.key)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertClaims != nil {
				tt.assertClaims(t, got)
			}
		})
	}
}

func TestClaims_Environment(t *testing.T) {
	tests := map[string]struct {
		c         Claims
		assertEnv func(*testing.T, []string)
	}{
		"standard structure": {
			c: Claims{
				UserID:    "123",
				UserLogin: "tester",
				UserEmail: "test@example.com",
				OverrideClaims: OverrideClaims{
					Issuer:  "example.com",
					Subject: "subject",
				},
			},
			assertEnv: func(t *testing.T, s []string) {
				assert.Contains(t, s, "JWT_USER_ID=123")
				assert.Contains(t, s, "JWT_USER_LOGIN=tester")
				assert.Contains(t, s, "JWT_ISS=example.com")
				assert.Contains(t, s, "JWT_SUB=subject")
			},
		},
		"user identities": {
			c: Claims{
				UserID: "123",
				UserIdentities: []map[string]string{
					{"provider": "https://test.example.com", "extern_uid": "1"},
					{"provider": "GITHUB ", "extern_uid": "123456789"},
				},
			},
			assertEnv: func(t *testing.T, s []string) {
				assert.Contains(t, s, "JWT_USER_ID=123")
				assert.Contains(t, s, "JWT_IDENTITY_HTTPSTESTEXAMPLECOM=1")
				assert.Contains(t, s, "JWT_IDENTITY_GITHUB=123456789")
			},
		},
		"multiple audiences": {
			c: Claims{
				UserID: "123",
				OverrideClaims: OverrideClaims{
					Audience: jwt.ClaimStrings{
						"https://gitlab.example.com",
						"127.0.0.1:80",
					},
				},
			},
			assertEnv: func(t *testing.T, s []string) {
				assert.Contains(t, s, "JWT_AUD=https://gitlab.example.com;127.0.0.1:80")
			},
		},
		"singe audience": {
			c: Claims{
				UserID: "123",
				OverrideClaims: OverrideClaims{
					Audience: jwt.ClaimStrings{
						"https://gitlab.example.com",
					},
				},
			},
			assertEnv: func(t *testing.T, s []string) {
				assert.Contains(t, s, "JWT_AUD=https://gitlab.example.com")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := tt.c.Environment()

			if tt.assertEnv != nil {
				tt.assertEnv(t, got)
			}
		})
	}
}

func TestClaims_jwtGetters(t *testing.T) {
	// Enforce that required claims interface works as expected that
	// are not used anywhere else.

	t.Run("GetIssuer", func(t *testing.T) {
		got, err := functional.GetIssuer()
		assert.Equal(t, got, functional.Issuer)
		assert.NoError(t, err)

	})

	t.Run("GetSubject", func(t *testing.T) {
		got, err := functional.GetSubject()
		assert.Equal(t, got, functional.Subject)
		assert.NoError(t, err)
	})

	t.Run("GetIssuedAt", func(t *testing.T) {
		got, err := functional.GetIssuedAt()
		assert.Equal(t, got, functional.IssuedAt)
		assert.NoError(t, err)
	})
}
