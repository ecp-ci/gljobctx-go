// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package gljobctx

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"os"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
)

/*
JWT test constants created using https://jwt.io and the public/private key pair
made available there at time of creation. This may change in the future so please
find the public/private key below. Note that these values are already observed in
existing JWKS endpoint mocks.
*/

const (
	jwtPrivate = `-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC7VJTUt9Us8cKj
MzEfYyjiWA4R4/M2bS1GB4t7NXp98C3SC6dVMvDuictGeurT8jNbvJZHtCSuYEvu
NMoSfm76oqFvAp8Gy0iz5sxjZmSnXyCdPEovGhLa0VzMaQ8s+CLOyS56YyCFGeJZ
qgtzJ6GR3eqoYSW9b9UMvkBpZODSctWSNGj3P7jRFDO5VoTwCQAWbFnOjDfH5Ulg
p2PKSQnSJP3AJLQNFNe7br1XbrhV//eO+t51mIpGSDCUv3E0DDFcWDTH9cXDTTlR
ZVEiR2BwpZOOkE/Z0/BVnhZYL71oZV34bKfWjQIt6V/isSMahdsAASACp4ZTGtwi
VuNd9tybAgMBAAECggEBAKTmjaS6tkK8BlPXClTQ2vpz/N6uxDeS35mXpqasqskV
laAidgg/sWqpjXDbXr93otIMLlWsM+X0CqMDgSXKejLS2jx4GDjI1ZTXg++0AMJ8
sJ74pWzVDOfmCEQ/7wXs3+cbnXhKriO8Z036q92Qc1+N87SI38nkGa0ABH9CN83H
mQqt4fB7UdHzuIRe/me2PGhIq5ZBzj6h3BpoPGzEP+x3l9YmK8t/1cN0pqI+dQwY
dgfGjackLu/2qH80MCF7IyQaseZUOJyKrCLtSD/Iixv/hzDEUPfOCjFDgTpzf3cw
ta8+oE4wHCo1iI1/4TlPkwmXx4qSXtmw4aQPz7IDQvECgYEA8KNThCO2gsC2I9PQ
DM/8Cw0O983WCDY+oi+7JPiNAJwv5DYBqEZB1QYdj06YD16XlC/HAZMsMku1na2T
N0driwenQQWzoev3g2S7gRDoS/FCJSI3jJ+kjgtaA7Qmzlgk1TxODN+G1H91HW7t
0l7VnL27IWyYo2qRRK3jzxqUiPUCgYEAx0oQs2reBQGMVZnApD1jeq7n4MvNLcPv
t8b/eU9iUv6Y4Mj0Suo/AU8lYZXm8ubbqAlwz2VSVunD2tOplHyMUrtCtObAfVDU
AhCndKaA9gApgfb3xw1IKbuQ1u4IF1FJl3VtumfQn//LiH1B3rXhcdyo3/vIttEk
48RakUKClU8CgYEAzV7W3COOlDDcQd935DdtKBFRAPRPAlspQUnzMi5eSHMD/ISL
DY5IiQHbIH83D4bvXq0X7qQoSBSNP7Dvv3HYuqMhf0DaegrlBuJllFVVq9qPVRnK
xt1Il2HgxOBvbhOT+9in1BzA+YJ99UzC85O0Qz06A+CmtHEy4aZ2kj5hHjECgYEA
mNS4+A8Fkss8Js1RieK2LniBxMgmYml3pfVLKGnzmng7H2+cwPLhPIzIuwytXywh
2bzbsYEfYx3EoEVgMEpPhoarQnYPukrJO4gwE2o5Te6T5mJSZGlQJQj9q4ZB2Dfz
et6INsK0oG8XVGXSpQvQh3RUYekCZQkBBFcpqWpbIEsCgYAnM3DQf3FJoSnXaMhr
VBIovic5l0xFkEHskAjFTevO86Fsz1C2aSeRKSqGFoOQ0tmJzBEs1R6KqnHInicD
TQrKhArgLXX4v3CddjfTRJkFWDbE/CkvKZNOrcf1nhaGCPspRJj2KUkj1Fhl9Cnc
dn/RsYEONbwQSjIfMPkvxF+8HQ==
-----END PRIVATE KEY-----

`
	jwtPublic = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1SU1LfVLPHCozMxH2Mo
4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0/IzW7yWR7QkrmBL7jTKEn5u
+qKhbwKfBstIs+bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyeh
kd3qqGElvW/VDL5AaWTg0nLVkjRo9z+40RQzuVaE8AkAFmxZzow3x+VJYKdjykkJ
0iT9wCS0DRTXu269V264Vf/3jvredZiKRkgwlL9xNAwxXFg0x/XFw005UWVRIkdg
cKWTjpBP2dPwVZ4WWC+9aGVd+Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbc
mwIDAQAB
-----END PUBLIC KEY-----

`
)

var functional Claims

type jwtTest struct {
	encoded   string
	jobID     string
	serverURL string

	claims Claims
	job    job
	key    *rsa.PublicKey
	opt    Options

	assertError  func(*testing.T, error)
	assertClaims func(*testing.T, Claims)
}

func TestOptions_ValidateJWT(t *testing.T) {
	tlsFile := t.TempDir() + "/ca.file"
	_ = os.WriteFile(tlsFile, []byte(`-----BEGIN CERTIFICATE-----
example...
-----END CERTIFICATE-----
`), 0700)

	tests := map[string]jwtTest{
		"unable to located tls file": {
			opt: Options{
				TLSCAFile: t.TempDir() + "/missing.cert",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "invalid TLS CA file supplied")
			},
		},
		"detect invalid delay": {
			opt: Options{
				TLSCAFile: tlsFile,
				ExpDelay:  maxDelay + 1*time.Minute,
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "configured ExpDelay surpasses maximum")
			},
		},
		"observe error in GET": {
			opt:       Options{},
			encoded:   generateJWT(functional),
			serverURL: "127.0.0.1",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "unable to retrieve key from JWKS endpoint")
				assert.True(t, errors.Is(err, ErrorJWKS), "errors.Is ErrorJWKS")
			},
		},
		"observe error in header": {
			opt: Options{
				TLSCAFile: tlsFile,
				ExpDelay:  maxDelay - 1*time.Minute,
			},
			encoded: func() string {
				token := jwt.NewWithClaims(jwt.SigningMethodHS256, functional)
				str, _ := token.SignedString([]byte("secret"))
				return str
			}(),
			serverURL: "https://gitlab.example.com",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "unable to validate JWT header")
			},
		},
		"public key provided": {
			opt: Options{
				JobID:     "123",
				PublicKey: generatePublicKey(),
			},
			encoded:   generateJWT(functional),
			serverURL: "127.0.0.1",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.opt.ValidateJWT(tt.encoded, tt.serverURL)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertClaims != nil {
				tt.assertClaims(t, got)
			}
		})
	}
}

func Test_Validator_GitLabJWT(t *testing.T) {
	encoded := os.Getenv("TEST_ID_JWT")
	server := os.Getenv("CI_SERVER_URL")
	jobID := os.Getenv("CI_JOB_ID")

	if encoded == "" && server == "" {
		t.Skip("required CI env not found")
	}

	t.Run("test full workflow during GitLab CI", func(t *testing.T) {
		claims, err := Options{
			JobID:            jobID,
			RequiredAudience: "https://gitlab.com/ecp-ci",
		}.ValidateJWT(encoded, server)

		assert.NoError(t, err)
		if err == nil {
			assert.Equal(t, jobID, claims.JobID)
			assert.Equal(t, claims.JobID, os.Getenv("CI_JOB_ID"))
			assert.Equal(t, claims.UserID, os.Getenv("GITLAB_USER_ID"))
			assert.Equal(t, claims.UserEmail, os.Getenv("GITLAB_USER_EMAIL"))
		}
	})

	t.Run("test error handling", func(t *testing.T) {
		_, err := Options{
			JobID:            jobID,
			RequiredAudience: "https://example.com",
		}.ValidateJWT(encoded, server)

		assert.Error(t, err)
	})

	t.Run("test retrieve public key", func(t *testing.T) {
		key, err := RetrievePublicKey(encoded, server)
		assert.NoError(t, err)
		assert.NotNil(t, key)
	})
}

func generateJWT(c Claims) string {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, c)
	token.Header["kid"] = "test"

	key, _ := jwt.ParseRSAPrivateKeyFromPEM([]byte(jwtPrivate))
	str, _ := token.SignedString(key)

	return str
}

func generatePublicKey() *rsa.PublicKey {
	block, _ := pem.Decode([]byte(jwtPublic))
	pub, _ := x509.ParsePKIXPublicKey(block.Bytes)
	return pub.(*rsa.PublicKey)
}

func init() {
	functional = Claims{
		UserLogin:     "user",
		ProjectPath:   "group/project",
		NamespaceID:   "1001",
		NamespacePath: "group",
		JobID:         "123",
		PipelineID:    "456",
		ProjectID:     "2002",
		Ref:           "066ac420faf6a3e96561eca998796af07480a64e",
		RefType:       "test",
		RefProtected:  "true",
		UserID:        "789",
		UserEmail:     "user@example.com",

		OverrideClaims: OverrideClaims{
			Subject: "job_123",
			ExpiresAt: &jwt.NumericDate{
				time.Now().Add(1 * time.Hour),
			},
			NotBefore: &jwt.NumericDate{
				time.Now(),
			},
			JwtId: "abc-123",
			Audience: jwt.ClaimStrings{
				"example.com",
			},
			Issuer: "issuer@example.com",
		},
	}
}

func TestOptions_build(t *testing.T) {
	tests := map[string]struct {
		opt       Options
		encoded   string
		serverURL string
		assertJob func(*testing.T, job)
	}{
		"example oauth discovery path": {
			opt:       Options{},
			serverURL: "https://example.gitlab.com/",
			encoded:   "example.test.jwt",
			assertJob: func(t *testing.T, j job) {
				assert.Equal(
					t,
					"https://example.gitlab.com/oauth/discovery/keys",
					j.jwksURL,
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := tt.opt.build(tt.encoded, tt.serverURL)

			if tt.assertJob != nil {
				tt.assertJob(t, got)
			}
		})
	}
}
